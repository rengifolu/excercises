
// https://www.freecodecamp.org/espanol/news/javascript-rest-vs-operador-de-propagacion-cual-es-la-diferencia/


console.log("**************************************************************")
console.log("*                                                            *")
console.log("* Ahora, considera este ejemplo del operador de propagación: *")
console.log("*                                                            *")
console.log("**************************************************************")


function miBiografiaPropagación(primerNombre, apellido, compania) {
    return `${primerNombre} ${apellido} dirige ${compania}`;
  }
  
console.log(miBiografiaPropagación(...['Oluwatobi', 'Sofela', 'CodeSweetly']));
  

 console.log("*********************************************************************************")
 console.log("*                                                                               *")
 console.log("* Ejemplo 1 de propagación: Cómo funciona la propagación en un arreglo literal: *")
 console.log("*                                                                               *")
 console.log("*********************************************************************************")

 const miNombre = ["Sofela", "es", "mi"];
 const sobreMi = ["Oluwatobi", ...miNombre, "nombre."];
 
 console.log("sobreMi");
 console.log(sobreMi);
 
 // La llamada de arriba devolverá:
 [ "Oluwatobi", "Sofela", "es", "mi", "nombre." ]

 /**
  * Las alteraciones de miNombre no se reflejarán en sobreMi porque todos los valores dentro de miNombre son primitivos. 
  * Por lo tanto, el operador de propagación simplemente copió y pegó el contenido de miNombre en sobreMi sin crear ninguna referencia al arreglo original.
  */

 /**
  * 
  * Supongamos que no utilizamos la sintaxis de propagación para duplicar el contenido de miNombre. Por ejemplo, 
  * 
  * si hubiéramos escrito const sobreMi = ["Oluwatobi", miNombre, "nombre."]. En tal caso, el ordenador habría asignado una referencia a miNombre. 
  * 
  * Así, cualquier cambio realizado en el arreglo original se reflejaría en el duplicado.
  * 
  */


  console.log("*********************************************************************************")
  console.log("*                                                                               *")
  console.log("* Ejemplo 2 de propagación: Cómo utilizar propagación para convertir            *")
  console.log("* una cadena en elementos individuales de un arreglo                            *")
  console.log("*********************************************************************************")

const miName = "Oluwatobi Sofela";

console.log([...miName]);

// La llamada de arriba devolverá:
// [ "O", "l", "u", "w", "a", "t", "o", "b", "i", " ", "S", "o", "f", "e", "l", "a" ]


console.log("*********************************************************************************")
console.log("*                                                                               *")
console.log("* Ejemplo 3: Cómo funciona el operador de propagación                           *")
console.log("* en una llamada de función                                                     *")
console.log("*********************************************************************************")


const numeros = [1, 3, 5, 7];

function agregarNumeros(a, b, c, d) {
  return a + b + c + d;
}

console.log(agregarNumeros(...numeros));

// La llamada de arriba devolverá:
// 16


/**
 * Supongamos que el arreglo de numeros tiene más de cuatro elementos. En tal caso, 
 * el ordenador sólo utilizará los cuatro primeros elementos como argumento de agregarNumeros() e ignorará el resto.
 * 
 */


 /**
  * Aqui un ejemplo:
  */
 const nummer = [1, 3, 5, 7, 10, 200, 90, 59];

 function agregarNumeros(a, b, c, d) {
   return a + b + c + d;
 }
 
 // console.log(agregarNumeros(...nummer));
 
 // La llamada de arriba devolverá:
 // 16



 /**
  * He aquí otro ejemplo:
  */


  const miNameb = "Oluwatobi Sofela";

  function deletrearNombre(a, b, c) {
    return a + b + c;
  }
  
/*   console.log(deletrearNombre(...miNameb));      // devolverá: "Olu"
  
  console.log(deletrearNombre(...miNameb[3]));   // devolverá: "wundefinedundefined"
  
  console.log(deletrearNombre([...miNameb]));    // devolverá: "O,l,u,w,a,t,o,b,i, ,S,o,f,e,l,aundefinedundefined"
  
  console.log(deletrearNombre({...miNameb}));    // devolverá: "[object Object]undefinedundefined" */




console.log("*********************************************************************************")
console.log("*                                                                               *")
console.log("* Ejemplo 4 de propagación: Cómo funciona la                                    *")
console.log("* propagación en un objeto literal                                              *")
console.log("*********************************************************************************")


const misNombres = ["Oluwatobi", "Sofela"];
const biografia = { ...misNombres, dirige: "codesweetly.com" };

console.log(biografia);

// La llamada de arriba devolverá:
// { 0: "Oluwatobi", 1: "Sofela", dirige: "codesweetly.com" }


/**
 * Información 1: Los operadores de propagación no pueden expandir los valores de los objetos literales
 * Dado que un objecto de propiedades no es un objeto iterable, no puedes utilizar el operador de propagación para expandir sus valores.
 * Sin embargo, puedes utilizar el operador de propagación para clonar propiedades de un objeto a otro.
 */


console.log("\n");
console.log("He aquí un ejemplo: \n");

const miNombreb = { primerNombre: "Oluwatobi", apellido: "Sofela" };
const biografiab = { ...miNombreb, sitioWeb: "codesweetly.com" };

console.log(biografiab);

// La llamada de arriba retornará:
// { primerNombre: "Oluwatobi", apellido: "Sofela", sitioWeb: "codesweetly.com" };

/**
 * Nota:
 *  El operador de propagación sólo puede expandir los valores de los objetos iterables.
 *  Un objeto es iterable sólo si él (o cualquier objeto en su cadena de prototipos) tiene una propiedad con una clave @@iterator.
 *  Array, TypedArray, String, Map, y Set son todos tipos iterables incorporados porque tienen la propiedad @@iterator por defecto.
 *  Un objeto no es un tipo de datos iterable porque no tiene la propiedad @@iterator por defecto.
 *  Puedes hacer que un objeto sea iterable añadiéndole @@iterator.
 */



/**
 * Información 2: El operador de propagación no clona propiedades idénticas
 * Supón que utilizas el operador de propagación para clonar propiedades del objeto A en el objeto B. 
 * Y supón que el objeto B contiene propiedades idénticas a las del objeto A. En tal caso, las versiones de B anularán las de A.
 */


 console.log("\n");
 console.log("He aquí un ejemplo: \n");

 const miNombrec = { primerNombre: "Tobi", apellido: "Sofela" };
 const biografiac = { ...miNombrec, primerNombre: "Oluwatobi", sitioWeb: "codesweetly.com" };
 
 console.log(biografiac);
 
 // La llamada de arriba devolverá:
 // { primerNombre: "Oluwatobi", apellido: "Sofela", sitioWeb: "codesweetly.com" };

 /**
  * Observe que el operador de propagación no ha copiado la propiedad miNombre de 
  * primerNombre en el objeto biografia porque biografia ya contiene una propiedad primerNombre.
  */



console.log("*********************************************************************************")
console.log("* Información 3: ¡Cuidado con el funcionamiento de                              *")
console.log("* propagación cuando se utiliza en objetos que no son                           *")
console.log("* primitivos!                                                                   *")
console.log("*********************************************************************************")

const miNombred = ["Sofela", "es", "mi"];
const sobreMid = ["Oluwatobi", ...miNombred, "nombre."];

console.log(sobreMid);

// La llamada de arriba devolverá:
// ["Oluwatobi", "Sofela", "es", "mi", "nombre."]

/**
 * Observa que cada elemento de miNombre es un valor primitivo. Por lo tanto, cuando usamos el operador de propagación para clonar miNombre en sobreMi, el ordenador no creó ninguna referencia entre los dos arreglos.
 *
 *  Como tal, cualquier alteración que hagas a miNombre no se reflejará en sobreMi, y viceversa.
 *
 *  Como ejemplo, vamos a añadir más contenido a miNombre:
 */

 miNombred.push("verdadero");


// Ahora, vamos a comprobar el estado actual de miNombre y sobreMi:

console.log(miNombred); // ["Sofela", "es", "mi", "verdadero"]

console.log(sobreMid); // ["Oluwatobi", "Sofela", "es", "mi", "verdadero."]

console.log("*********************************************************************************")


/**
 * ¿Qué pasa si miNombre contiene elementos no primitivos?
 */



 const miNombreE = [["Sofela", "es", "mi"]];
 const sobreMiE = ["Oluwatobi", ...miNombreE, "nombre."];
 
 console.log(sobreMiE);
 
 // La llamada de arriba retornará:
 [ "Oluwatobi", ["Sofela", "es", "mi"], "nombre." ]

 /**
  * Observe que miNombre contiene un valor no primitivo .
  *
  *  Por lo tanto, el uso del operador de propagación para clonar el contenido de miNombre en sobreMi hizo que el ordenador creara una referencia entre los dos arreglos.
  *
  *  Como tal, cualquier alteración que haga en la copia de miNombre se reflejará en la versión de sobreMi, y viceversa.
  *
  *  Como ejemplo, vamos a añadir más contenido a miNombre:
  */


  miNombreE[0].push("verdadero");


//   Ahora, vamos a comprobar el estado actual de miNombre y sobreMi:

console.log(miNombreE); // [["Sofela", "es", "mi", "verdadero"]]

console.log(sobreMiE); // ["Oluwatobi", ["Sofela", "es", "mi", "verdadero"], "nombre."]

console.log("\nAquí hay otro ejemplo:\n"); 

const miNombreF = { primerNombre: "Oluwatobi", apellido: "Sofela" };
const biografiaF = { ...miNombreF };

miNombreF.primerNombre = "Tobi";

console.log(miNombreF); // { primerNombre: "Tobi", apellido: "Sofela" }

console.log(biografiaF); // { primerNombre: "Oluwatobi", apellido: "Sofela" }

/**
 * En el fragmento anterior, la actualización de miNombre no se reflejó en biografia porque utilizamos 
 * el operador de propagación en un objeto que sólo contiene valores primitivos.
 * Nota: Un desarrollador llamaría a miNombre un objeto profundo porque contiene sólo elementos primitivos.
 */


 console.log("\nAquí hay un ejemplo más:\n"); 


 const miNombreG = { nombreCompleto: { primerNombre: "Oluwatobi", apellido: "Sofela" }};

const biografiaG = { ...miNombreG };

miNombreG.nombreCompleto.primerNombre = "Tobi";

console.log(miNombreG); // { nombreCompleto: { primerNombre: "Tobi", apellido: "Sofela" } }

console.log(biografiaG); // { nombreCompleto: { primerNombre: "Tobi", apellido: "Sofela" } }


/**
 * En el fragmento anterior, la actualización de miNombre se refleja en biografia porque hemos 
 * utilizado el operador de propagación en un objeto que contiene un valor no primitivo.
 */

/**
 * Nota:
 * Llamamos a miNombre objeto profundo porque contiene un elemento no primitivo.
 * La copia profunda se realiza cuando se crean referencias al clonar un objeto en otro. Por ejemplo, ...miNombre produce una copia profunda 
 * del objeto miNombre porque cualquier alteración que hagas en uno se reflejará en el otro.
 * La copia profunda se realiza cuando se clonan objetos sin crear referencias. Por ejemplo, podría copiar miNombre en biografia 
 * haciendo const biografia = JSON.parse(JSON.stringify(miNombre)). Haciendo esto, el ordenador clonará miNombre en biografia sin crear ninguna referencia.
 * Puedes romper la referencia entre los dos objetos reemplazando el objeto nombreCompleto dentro de miNombre o biografia con un nuevo objeto. 
 * Por ejemplo, haciendo miNombre.nombreCompleto = { primerNombre: "Tobi", apellido: "Sofela" } desconectaría el puntero entre miNombre y biografia.
 */