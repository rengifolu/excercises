
 console.log("***********************************************")
 console.log("*                                             *")
 console.log("* JavaScript rest vs. operador de propagación *")
 console.log("*                                             *")
 console.log("***********************************************")
 
 /**
  * La principal diferencia entre rest y el operador propagación 
  * es que el operador rest pone el resto de algunos valores específicos suministrados por el usuario en un arreglo de JavaScript. 
  * Pero la progración expande los iterables en elementos individuales.
  * */ 
 
 
 // Utiliza rest para encerrar el resto de los valores específicos suministrados por el usuario en un arreglo:
 function miBiografia(primerNombre, apellido, ...otraInformacion) { 
     return otraInformacion;
   }
   
   // Llama la función miBiografia pasando cinco argumentos a sus parámetros:   pero solo devuelve en un array el resto osea los tre strings ultimos
console.log( miBiografia("Oluwatobi", "Sofela", "CodeSweetly", "Desarrollador Web", "Masculino"));
// si se utiliza rest en un arreglo o función de desestructuración el operador producirá un arreglo literal.




/** ¡Cuidado! No puedes utilizar “use strict” dentro de una función que contenga un parámetro rest */

// Define una función con un parámetro res:
function imprimirMiNombre(...valor) {
   // "use strict";
    return valor;
  }
  
  // La definición anterior devolverá:
  // "Uncaught SyntaxError: Illegal 'use strict' directive in function with non-simple parameter list"



// Define un objeto de desestructuración con dos variables regulares y una variable res:
const { primerNombre, apellido, ...otraInformacion } = {
    primerNombre: "Oluwatobi",
    apellido: "Sofela", 
    nombreCompania: "CodeSweetly",
    profesion: "Desarrollador Web",
    genero: "Masculino"
  }
  
  // Llama a la variable otraInformacion:
  console.log(otraInformacion);
  
  // La llamada de arriba devolverá:
  //{nombreCompania: "CodeSweetly", profesion: "Desarrollador Web", genero: "Masculino"}

  /**
   * 
   * En el fragmento anterior, observa que el operador rest asignó un objeto de propiedades - no un arreglo - a la variable otraInformacion.
   *
   * En otras palabras, siempre que utilice rest en un objeto desestructurado el operador rest producirá un objeto de propiedades.
   *
   * Sin embargo, si se utiliza rest en un arreglo o función de desestructuración el operador producirá un arreglo literal.
   */