


const callbac = function() {  
    console.log("Este mensaje se muestra después de caca  segundos");
}

function imprimir(callbac) {  
    callbac();
    console.log("--")
}
imprimir(callbac)

/**
 * Los callbacks aseguran que una función no se va a ejecutar antes de que se complete una tarea, sino que se ejecutará 
 * justo después de que la tarea se haya completado. Nos ayuda a desarrollar código JavaScript asíncrono y nos mantiene 
 * a salvo de problemas y errores.
 */


/**
 * En JavaScript, la forma de crear una función callback es pasándola como parámetro a otra función, y luego llamarla de vuelta 
 * justo después de que haya ocurrido algo o se haya completado alguna tarea. Veamos cómo..
 */


 const mensaje = function() {  
    console.log("Este mensaje se muestra después de 3 segundos");
}
 
setTimeout(mensaje, 3000);




/**
 * ¿Qué es una función anónima?
 *
 */

 setTimeout(function() {  
    console.log("Este mensaje se muestra después de ccccc segundos");
}, 5000);



/**
 * Callback en forma de función de flecha
 */

 setTimeout(() => { 
    console.log("Este mensaje se muestra después de 3 segundos");
}, 3000);

/**
 * Si lo prefiere, también puede escribir la misma función callback como una función de flecha ES6, que es un tipo de función más reciente en JavaScript:
 */



/**
 * ¿Qué pasa con los eventos?
 * JavaScript es un lenguaje de programación basado en eventos. También utilizamos funciones callbacks para las declaraciones de eventos. 
 * Por ejemplo, digamos que queremos que los usuarios hagan clic en un botón:
 */

<button id="callback-btn">Haga clic aquí</button>

document.queryselector("#callback-btn")
    .addEventListener("click", function() {    
      console.log("El usuario ha hecho clic en el botón.");
});

/**
 * Así que aquí seleccionamos el botón primero con su id, y luego añadimos un receptor de eventos con el método addEventListener. 
 * Toma 2 parámetros. El primero es su tipo, "click", y el segundo parámetro es una función callback, que registra el mensaje cuando el botón es pulsado.
 * Como puedes ver, las funciones callback también se utilizan para las declaraciones de eventos en JavaScript.
 */