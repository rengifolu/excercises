
# TDD

# crea estructura basica
npm init -y  

# instala jest
npm i -D jest

# para configurar jest
npx jest --init


# asi se inicia tets 
npm test

# asi se ejecuta con watch
npm run test:watch


# de esta manera ejecutamos solo la clase completa osea el fichro con todas sus funciones sin llamar al test  node <nombre del export fizzbuzz>
node fizzbuzz