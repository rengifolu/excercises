function fizzbuzz (num){

    /*comprueba si lo que se ingresa es de tipo number osea un numero*/ 
    if(typeof num !== "number") {
        return "Error the number must be of type number"
    }

    if(num === 0) {
        return 0
    }

    /*esta prueba la ponemos al ocmienzo para que haga las dos comprobaciones sino cogeria el que e smodeulo de tres al ser el primero que encontraria*/ 
    if(num % 3 === 0 && num % 5 === 0){
        return "fizzbuzz"
    }


/*     if(num === 5){
        return "buzz"
    } */

    if(num % 3 === 0){
        return "fizz"
    }


    if(num % 5 === 0){
        return "buzz"
    }

    return num
}


function print (num){
    for (let index = 0; index < num; index++) {
         console.log(index  + " " + fizzbuzz(index))
    }
}

print(16)
/*siempre hay que exportarlo*/ 
module.exports = fizzbuzz