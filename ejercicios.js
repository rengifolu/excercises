function multiplicar(){
let arr = [3, 4, 5, 6];

for (let i = 0; i < arr.length; i++){
  arr[i] = arr[i] * 3;
}

console.log(arr); // [9, 12, 15, 18]
}

multiplicar()


function multiplicarConMap(){
    let arr = [10, 4, 50, 63];

    let modifiedArr = arr.map(function(element){
        return element *3;
    });
    
    console.log(modifiedArr); 
}

multiplicarConMap()


// Cómo utilizar map() sobre un arreglo de objetos
console.log("***********************************************")

console.log("Cómo utilizar map() sobre un arreglo de objetos")
let users = [
    {firstName : "Susan", lastName: "Steward"},
    {firstName : "Daniel", lastName: "Longbottom"},
    {firstName : "Jacob", lastName: "Black"}
  ];

  let userFullnames = users.map(function(element){
    return `${element.firstName} ${element.lastName}`;
    })

console.log(userFullnames);
// ["Susan Steward", "Daniel Longbottom", "Jacob Black"]


console.log("***********************************************")
console.log("La sintaxis completa del método map()   tiene tres parametros por defecto")
let userFullname = users.map(function(element, index, array){
    return `${element.firstName} // ${element.lastName}  // ${index}  // ${array}`;
    })
 console.log(userFullname);



module.exports = multiplicar


