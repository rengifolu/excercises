const fizzbuzz = require ("./fizzbuzz")


describe("fizzbuzz", () => {
/*     test("test", () => {
        expect(true).toBe(true)
    }) */


    test("should receive a number type", () => {
        const expected = "Error the number must be of type number"
        const result = fizzbuzz("test not tpe number")
        expect(expected).toBe(result)
    })

    test("should print 1 if receive 1", () => {
        const expected = 1
        const result = fizzbuzz(1)
        expect(expected).toBe(result)
    })

    test("should print fezz if receive a multiple of 3", () => {
        const expected ="fizz"
        const result = fizzbuzz(3)
        expect(expected).toBe(result)
    })

    test("should print fezz if receive a multiple of 3", () => {
        const expected ="fizz"
        const result = fizzbuzz(6)
        expect(expected).toBe(result)
    })

    test("should print buzz if receive a multiple of 5", () => {
        const expected ="buzz"
        const result = fizzbuzz(5)
        expect(expected).toBe(result)
    })

    test("should print buzz if receive a multiple of 5", () => {
        const expected ="buzz"
        const result = fizzbuzz(10)
        expect(expected).toBe(result)
    })


    test("should print fizzbuzz if receive a multiple of 3 an 5", () => {
        const expected ="fizzbuzz"
        const result = fizzbuzz(15)
        expect(expected).toBe(result)
    })


})